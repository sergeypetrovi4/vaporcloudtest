

import Vapor

public func routes(_ router: Router) throws {
    
    router.get("hello", use: helloName)
    
    let categoriesController = CategoriesController()
    router.get("categories", use: categoriesController.categories)
    router.get("file", use: categoriesController.file)
    
    router.post(Category.self, at: "category/create") { (req, data) -> Future<Category> in
        return categoriesController.create(req, data: data)
    }
    
//    let routerGroup = router.grouped("group")
//    routerGroup.get("documents") { request in
//        return "Group is Documents"
//    }
//
//    routerGroup.get("keys") { request in
//        return "Group is Keys"
//    }
}

// MARK: - Private

private func helloName(_ request: Request) throws -> String {

    guard let name = try? request.query.get(String.self, at: "name"),
          let age = try? request.query.get(Int.self, at: "age") else {
        return "Wrong parameters type!"
    }

    return "Hello \(name), with age \(age)"
}





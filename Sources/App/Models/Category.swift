//
//  Category.swift
//  App
//
//  Created by Sergey Krasiuk on 13/04/2019.
//

import Vapor

final class Category {
    
    enum CategoryIdentifier: String, CaseIterable {
        
        case documents
        case keys
        case valuable
        case pets
        case transport
        case electronics
        case clothes
        case tools
        case other
    }
    
    var icon: String
    var title: String
    var categoryId: Int
    
    init(categoryId: CategoryIdentifier) {
        
        self.title = categoryId.rawValue
        self.icon = categoryId.rawValue
        self.categoryId = CategoryIdentifier.allCases.index(of: categoryId)!
    }
}

extension Category: Content {}

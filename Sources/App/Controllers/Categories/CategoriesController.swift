//
//  CategoriesController.swift
//  App
//
//  Created by Sergey Krasiuk on 17/04/2019.
//

import Vapor

final class CategoriesController {
    
    func categories(_ req: Request) throws -> Future<[Category]> {
        return Future.map(on: req, { () -> [Category] in
            return [
                Category(categoryId: .documents),
                Category(categoryId: .keys),
                Category(categoryId: .valuable),
                Category(categoryId: .pets),
                Category(categoryId: .transport),
                Category(categoryId: .electronics),
                Category(categoryId: .clothes),
                Category(categoryId: .tools),
                Category(categoryId: .other)
            ]
        })
    }
    
    func file(_ req: Request) throws -> Response {
        
        let directory = DirectoryConfig.detect()
//        req.response.http.status = .ok
//        req.response.http.contentType = .png
//
//        let length = response.http.headers["content-length"].first!
//
//        if let data = try? req.fileio().chunkedResponse(file: "\(directory.workDir)Public/monster.png") {
//            return req.response(data, as: .png)
//        }
        
        let url = URL(fileURLWithPath: "\(directory.workDir)monster.png")
        
        if let data = try? Data(contentsOf: url) {
            return req.response(data, as: .png)
        }

        return req.response("Image not available")
    }
    
    func create(_ req: Request, data: Category) -> Future<Category> {
        return Future.map(on: req, { () -> Category in
            data.title = "Re-new category"
            data.categoryId = 800
            data.icon = "Re-new icon"
            return data
        })
    }
}
